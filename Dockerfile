FROM python:3.5.2
ADD . /code
WORKDIR /code
ENV PYTHONUNBUFFERED 0
RUN pip install -r requirements.txt
RUN python setup.py develop