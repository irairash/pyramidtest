How to run the app?
To run the app go to the root directory of the app and:
- docker build -t pyramidtest_image .
- docker-compose run web python setup.py develop
- docker-compose up

To run the tests:
- docker-compose run web python setup.py test

The application must be running on http://0.0.0.0:6543/


---
Documentation : https://app.swaggerhub.com/apis/ira55/pyramidtest/1.0.0

---
/v1/calculate

Possible calls:

curl -i -H "Accept: application/json" -H "Content-Type: application/json" -X GET 0.0.0.0:6543/v1/calculate/3+2*3 (Get the result of calculation)
