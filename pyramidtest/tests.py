import unittest

from pyramid import testing


class ViewTests(unittest.TestCase):
    def setUp(self):
        self.config = testing.setUp()

    def tearDown(self):
        testing.tearDown()

    def test_my_view(self):
        from .views.default import my_view
        request = testing.DummyRequest()
        info = my_view(request)
        self.assertEqual(info['project'], 'pyramid-test')


class FunctionalTests(unittest.TestCase):
    def setUp(self):
        from pyramidtest import main
        app = main({})
        from webtest import TestApp
        self.testapp = TestApp(app)

    def test_root(self):
        res = self.testapp.get('/', status=200)
        self.assertTrue(b'pyramid' in res.body)

    def test_calculate(self):
        result = self.testapp.get('/v1/calculate/3 + 2', status=200)
        self.assertEqual(result.status_code, 200)
        self.assertEqual(result.json_body['result'], 5)

    def test_calculate_wrong_method(self):
        result = self.testapp.post('/v1/calculate/3 + 2', status=200)
        self.assertEqual(result.json_body['status'], 404)

    def test_calculate_wrong_parameter(self):
        result = self.testapp.get('/v1/calculate/3 + 2a', status=200)
        self.assertEqual(result.status_code, 200)
        self.assertEqual(result.json_body['status'], 500)
        self.assertEqual(result.json_body['result'], None)

