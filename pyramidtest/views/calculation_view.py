import re

from pyramid.view import view_config


@view_config(route_name='calculate', request_method='GET', renderer='json')
def calculate(request):
    equation = request.matchdict.get('parameter')

    # Validate the input
    if not bool(re.match("^[0-9 +*%/\-()]+$", equation)):

        return {'status': 500, 'result': None,
                'message': 'Wrong input. Only integers,'
                           ' -, +, *, /, % and () are accepted.'}
    # Calculate the result
    try:
        return {'result': eval(equation), 'message': 'Success', 'status': 200}
    except:
        return {'result': None, 'message': 'Calculation Error', 'status': 500}
