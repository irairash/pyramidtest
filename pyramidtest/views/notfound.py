from pyramid.view import notfound_view_config


@notfound_view_config(renderer='json')
def notfound_view(request):
    response = {'status': 404, 'result': None,
                'message': 'Wrong Http method.'}
    return response
